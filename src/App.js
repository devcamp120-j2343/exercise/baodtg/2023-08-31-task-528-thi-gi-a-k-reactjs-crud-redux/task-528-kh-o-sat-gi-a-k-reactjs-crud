import './App.css';
import { OrdersTable } from './components/OrdersTable';

function App() {
  return (
    <div>
    <OrdersTable/>
    </div>
  );
}

export default App;
