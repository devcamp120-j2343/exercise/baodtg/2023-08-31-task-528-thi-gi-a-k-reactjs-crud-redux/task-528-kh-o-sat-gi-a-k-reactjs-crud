import { ADD_ORDER_FETCH_ERROR, ADD_ORDER_FETCH_PENDING, ADD_ORDER_FETCH_SUCCESS } from "../constant/order.constant";


export const fetchAddOrder = (paramOrderObj) => {
    return async (dispatch) => {
        try {
            var myHeaders = new Headers();
            myHeaders.append("Content-Type", "application/json");

            var raw = JSON.stringify(paramOrderObj);

            var requestOptions = {
                method: 'POST',
                headers: myHeaders,
                body: raw,
                redirect: 'follow'
            };
            await dispatch({
                type: ADD_ORDER_FETCH_PENDING
            })
            // Gọi để lấy tổng số users có trong CSDL
            const responseAddOrders = await fetch("http://203.171.20.210:8080/devcamp-pizza365/orders", requestOptions);

            const dataNewOders = await responseAddOrders.json();
            console.log(dataNewOders)

            return dispatch({
                type: ADD_ORDER_FETCH_SUCCESS,
                data: dataNewOders
            })
        } catch (error) {
            return dispatch({
                type: ADD_ORDER_FETCH_ERROR,
                error: error
            })
        }
    }
}