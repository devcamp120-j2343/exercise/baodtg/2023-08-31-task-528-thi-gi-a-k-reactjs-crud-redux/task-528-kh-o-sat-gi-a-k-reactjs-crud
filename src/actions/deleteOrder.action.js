import { DELETE_ORDER_FETCH_ERROR, DELETE_ORDER_FETCH_PENDING, DELETE_ORDER_FETCH_SUCCESS } from "../constant/order.constant";

export const fetchDeleteOrder = (paramOrderObj) => {
    return async (dispatch) => {
        try {
            var requestOptions = {
                method: 'DELETE',
                redirect: 'follow'
            };
            await dispatch({
                type: DELETE_ORDER_FETCH_PENDING
            })
            const responseOrderDeleted = await fetch("http://203.171.20.210:8080/devcamp-pizza365/orders/" + paramOrderObj.id, requestOptions);

            if (responseOrderDeleted) {
                return dispatch({
                    type: DELETE_ORDER_FETCH_SUCCESS
                })
            }

        } catch (error) {
            return dispatch({
                type: DELETE_ORDER_FETCH_ERROR,
                error: error
            })
        }


    }
}