import { DRINK_FETCH_ERROR, DRINK_FETCH_PENDING, DRINK_FETCH_SUCCESS } from "../constant/order.constant";


export const fetchLoadDrinkList = () => {
    return async (dispatch) => {
        try {
            var requestOptions = {
                method: 'GET',
                redirect: 'follow'
            };
            await dispatch({
                type: DRINK_FETCH_PENDING
            })
            // Gọi để lấy tổng số users có trong CSDL
            const responseDrinks = await fetch("http://203.171.20.210:8080/devcamp-pizza365/drinks", requestOptions);

            const dataDrinks = await responseDrinks.json();

            return dispatch({
                type: DRINK_FETCH_SUCCESS,
                data: dataDrinks
            })
        } catch (error) {
            console.log(error)
            return dispatch({
                type: DRINK_FETCH_ERROR,
                error: error
            })
        }
    }
}