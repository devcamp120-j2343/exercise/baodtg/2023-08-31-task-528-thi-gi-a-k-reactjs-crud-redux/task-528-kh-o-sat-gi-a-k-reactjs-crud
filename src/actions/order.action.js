import { ORDER_FETCH_ERROR, ORDER_FETCH_PENDING, ORDER_FETCH_SUCCESS } from "../constant/order.constant";


export const fetchOrder = () => {
    return async (dispatch) => {
        try {
            var requestOptions = {
                method: "GET"
            }
            await dispatch({
                type: ORDER_FETCH_PENDING
            })
            // Gọi để lấy tổng số users có trong CSDL
            const responseOrders = await fetch("http://203.171.20.210:8080/devcamp-pizza365/orders", requestOptions);

            const dataOders = await responseOrders.json();
            
            return dispatch({
                type: ORDER_FETCH_SUCCESS,
                data: dataOders
            })
        } catch (error) {
            return dispatch({
                type: ORDER_FETCH_ERROR,
                error: error
            })
        }
    }
}