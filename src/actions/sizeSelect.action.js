import { L_SIZE_SELECTED, M_SIZE_SELECTED, S_SIZE_SELECTED } from "../constant/order.constant"


export const sizeSelectAction = (size) => {

    if (size === "S") {
        return ({
            type: S_SIZE_SELECTED,
            payload: {
                kichCo: "S",
                duongKinh: 20,
                suon: 2,
                salad: 200,
                soLuongNuoc: 2,
                thanhTien: 150000
            }
        })
    } else if (size === "M") {
        return ({
            type: M_SIZE_SELECTED,
            payload: {
                kichCo: "M",
                duongKinh: 25,
                suon: 4,
                salad: 300,
                soLuongNuoc: 3,
                thanhTien: 200000
            }
        })
    } else if (size === "L") {
        return ({
            type: L_SIZE_SELECTED,
            payload: {
                kichCo: "L",
                duongKinh: 30,
                suon: 8,
                salad: 500,
                soLuongNuoc: 4,
                thanhTien: 250000
            }
        })
    }

}