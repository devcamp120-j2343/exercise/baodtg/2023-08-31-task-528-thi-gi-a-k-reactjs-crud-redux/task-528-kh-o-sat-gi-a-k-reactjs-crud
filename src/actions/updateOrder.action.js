import { UPDATE_ORDER_FETCH_ERROR, UPDATE_ORDER_FETCH_SUCCESS } from "../constant/order.constant";


export const fetchUpdateOrder = (paramOrderObj, paramOrderStatus) => {


    return async (dispatch) => {
        try {
            var myHeaders = new Headers();
            myHeaders.append("Content-Type", "application/json");

            var raw = JSON.stringify({
                "trangThai": paramOrderStatus
            });
            var requestOptions = {
                method: 'PUT',
                headers: myHeaders,
                body: raw,
                redirect: 'follow'
            };
            await dispatch({
                type: 'UPDATE_ORDER_FETCH_PENDING'
            })
            const responseOrderUpdated = await fetch("http://203.171.20.210:8080/devcamp-pizza365/orders/" + paramOrderObj.id, requestOptions);

            const dataOrderUpdated = await responseOrderUpdated.json();
            return dispatch({
                type: UPDATE_ORDER_FETCH_SUCCESS,
                data: dataOrderUpdated
            })
        } catch (error) {
            return dispatch({
                type: UPDATE_ORDER_FETCH_ERROR,
                error: error
            })
        }
    }
}