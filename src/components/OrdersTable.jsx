import { Alert, Box, Button, CircularProgress, Container, FormControl, Grid, InputLabel, MenuItem, Modal, Select, Snackbar, Stack, TextField, Typography } from "@mui/material"
import React, { useEffect } from "react";
import { useDispatch, useSelector } from "react-redux";
import { fetchOrder } from "../actions/order.action";
import { sizeSelectAction } from "../actions/sizeSelect.action";
import { fetchAddOrder } from "../actions/addOrder.action";
import { fetchLoadDrinkList } from "../actions/loadDrinkList.action";
import { fetchUpdateOrder } from "../actions/updateOrder.action";
import { GET_ORDER_WILL_BE_DELETE, GET_ORDER_WILL_BE_UPDATE, ORDER_UPDATE_DATA } from "../constant/order.constant";
import { fetchDeleteOrder } from "../actions/deleteOrder.action";
import { DataGrid } from "@mui/x-data-grid";

var vObjectRequest = {
    kichCo: "",
    duongKinh: 0,
    suon: 0,
    salad: 0,
    loaiPizza: "",
    idVourcher: "",
    idLoaiNuocUong: "",
    soLuongNuoc: 0,
    hoTen: "",
    thanhTien: 0,
    email: "",
    soDienThoai: "",
    diaChi: "",
    loiNhan: ""
}
export const OrdersTable = () => {
    const dispatch = useDispatch();

    const { orders, pending } = useSelector((reduxData) => reduxData.orderReducer);
    const { sizeSelected } = useSelector((reduxData) => reduxData.sizeReducer);
    const { newOrder, addOrderPending } = useSelector((reduxData) => reduxData.addOrderReducer);
    const { drinks, drinkPending } = useSelector((reduxData) => reduxData.drinkReducer);
    const { updateOrder, orderStatus, orderUpdated } = useSelector((reduxData) => reduxData.updateOrderReducer);
    const { deleteOrder, orderDeleted, deleteOrderPending } = useSelector((reduxData) => reduxData.deleteOrderReducer);


    useEffect(() => {
        dispatch(fetchOrder())
    }, [])

    const style = {
        position: 'absolute',
        top: '50%',
        left: '50%',
        transform: 'translate(-50%, -50%)',
        width: 700,
        bgcolor: 'background.paper',
        border: '2px solid #000',
        boxShadow: 24,
        p: 4,
    };
    const [open, setOpen] = React.useState(false);

    const [drink, setDrink] = React.useState('');
    const handleOpen = () => {
        setOpen(true);
        dispatch(fetchLoadDrinkList())
    };
    const drinkSelectHandler = (event) => {
        setDrink(event.target.value);
    }
    const handleClose = () => setOpen(false);


    const [size, setSize] = React.useState('');
    const handleChange = (event) => {
        setSize(event.target.value);
        dispatch(sizeSelectAction(event.target.value))
    };


    const [pizzaType, setPizzaType] = React.useState('');
    const pizzaTypeSelectHandler = (event) => {
        setPizzaType(event.target.value)
    }

    const [openSnackbarValidateAddOrder, setOpenSnackbarValidateAddOrder] = React.useState(false);


    const handleCloseSnackbarValidateAddOrder = () => {
        setOpenSnackbarValidateAddOrder(false)
    }
    const [validateAlert, setValidateAlert] = React.useState("");

    const validateDataAddOrder = () => {
        if (vObjectRequest.kichCo === undefined) {
            setOpenSnackbarValidateAddOrder(true);
            setValidateAlert("Vui lòng chọn Size Pizza!")

            return false
        }
        else if (vObjectRequest.loaiPizza === "") {
            setOpenSnackbarValidateAddOrder(true);
            setValidateAlert("Vui lòng chọn Loại Pizza!")
            return false
        }

        else if (isNaN(document.querySelector('#inp-id-voucher').value)) {
            setOpenSnackbarValidateAddOrder(true);
            setValidateAlert("Voucher phải là số!")
            return false
        }
        else if (vObjectRequest.idLoaiNuocUong === "") {
            setOpenSnackbarValidateAddOrder(true);
            setValidateAlert("Vui lòng chọn Loại Nước Uống!")
            return false
        }
        else if (document.querySelector('#inp-full-name').value === "") {
            setOpenSnackbarValidateAddOrder(true);
            setValidateAlert("Vui lòng chọn nhập Họ và Tên!")
            return false
        }
        else if (document.querySelector('#inp-phone').value === "") {
            setOpenSnackbarValidateAddOrder(true);
            setValidateAlert("Vui lòng chọn nhập Số Điện Thoại")
            return false
        }
        else if (document.querySelector('#inp-address').value === "") {
            setOpenSnackbarValidateAddOrder(true);
            setValidateAlert("Vui lòng chọn nhập Địa Chỉ!")
            return false
        }
        return true
    }
    const onBtnConfirmAddOrderClick = () => {
        vObjectRequest.kichCo = sizeSelected.kichCo;
        vObjectRequest.duongKinh = sizeSelected.duongKinh;
        vObjectRequest.suon = sizeSelected.suon;
        vObjectRequest.salad = sizeSelected.salad;
        vObjectRequest.soLuongNuoc = sizeSelected.soLuongNuoc;
        vObjectRequest.thanhTien = sizeSelected.thanhTien;
        vObjectRequest.loaiPizza = pizzaType;
        vObjectRequest.idVourcher = document.querySelector('#inp-id-voucher').value;
        vObjectRequest.idLoaiNuocUong = drink;
        vObjectRequest.hoTen = document.querySelector('#inp-full-name').value;
        vObjectRequest.email = document.querySelector('#inp-email').value;
        vObjectRequest.soDienThoai = document.querySelector('#inp-phone').value;
        vObjectRequest.diaChi = document.querySelector('#inp-address').value;
        vObjectRequest.loiNhan = document.querySelector('#inp-message').value;

        if (validateDataAddOrder()) {
            dispatch(fetchAddOrder(vObjectRequest))
            addedOrderHandler()

        }

    }
    const [openSnackbar, setOpenSnackbar] = React.useState(false);
    const addedOrderHandler = () => {
        if (newOrder !== []) {
            setOpenSnackbar(true);
            setOpen(false);
        }
    }
    const handleCloseSnackbar = () => {
        setOpenSnackbar(false);
        dispatch(fetchOrder())
    }



    const [openModalUpdateOrder, setOpenModalUpdateOrder] = React.useState(false);
    const handleCloseModalUpdateOrder = () => setOpenModalUpdateOrder(false);

    const orderStatusChangeHandler = (event) => {
        dispatch({
            type: ORDER_UPDATE_DATA,
            payload: event.target.value
        })
    }

    const onBtnConfirmUpdateOrderClick = () => {
        dispatch(fetchUpdateOrder(updateOrder, orderStatus));
        updatedOrderHandler();
    }
    const [openSnackbarUpdateOrder, setOpenSnackbarUpdateOrder] = React.useState(false);
    const updatedOrderHandler = () => {

        if (orderUpdated !== []) {
            setOpenModalUpdateOrder(false)

            setOpenSnackbarUpdateOrder(true);
        }
    }
    const handleCloseSnackbarUpdateOrder = () => {
        setOpenSnackbarUpdateOrder(false);
        dispatch(fetchOrder())
    }

    const [openModalDeleteOrder, setOpenModalDeleteOrder] = React.useState(false);
    const handleCloseModalDeleteOrder = () => setOpenModalDeleteOrder(false);

    const onBtnConfirmDeleteOrderClick = () => {
        dispatch(fetchDeleteOrder(deleteOrder));
        deletedOrderHandler();
    }
    const [openSnackbarDeleteOrder, setOpenSnackbarDeleteOrder] = React.useState(false);
    const deletedOrderHandler = () => {

        setOpenModalDeleteOrder(false)

        setOpenSnackbarDeleteOrder(true);

    }
    const handleCloseSnackbarDeleteOrder = () => {
        setOpenSnackbarDeleteOrder(false);

        dispatch(fetchOrder())
    }

    const columns = [
        { field: 'id', headerName: 'ID', width: 70 },
        { field: 'orderCode', headerName: 'Order Code', width: 130 },
        { field: 'kichCo', headerName: 'Kích cỡ', width: 130 },
        { field: 'loaiPizza', headerName: 'Loại Pizza', width: 130 },
        { field: 'idVourcher', headerName: 'Id voucher', width: 130 },
        { field: 'thanhTien', headerName: 'Thành tiền', width: 130 },
        { field: 'giamGia', headerName: 'Giảm giá', width: 130 },
        { field: 'hoTen', headerName: 'Họ và tên', width: 130 },
        { field: 'email', headerName: 'Email', width: 130 },
        { field: 'soDienThoai', headerName: 'Số điện thoại', width: 130 },
        { field: 'diaChi', headerName: 'Địa chỉ ', width: 130 },
        { field: 'loiNhan', headerName: 'Lời nhắn', width: 130 },
        { field: 'trangThai', headerName: 'Trạng thái', width: 130 },

        {
            field: 'action',
            headerName: 'Action',
            width: 180,
            sortable: false,
            disableClickEventBubbling: true,

            renderCell: (params) => {
                const onBtnUpdateClickHandler = (e) => {
                    const currentRow = params.row;
                    dispatch({
                        type: GET_ORDER_WILL_BE_UPDATE,
                        payload: currentRow
                    })
                    setOpenModalUpdateOrder(true)
                };
                const onBtnDeleteClickHandler = (e) => {
                    const currentRow = params.row;
                    dispatch({
                        type: GET_ORDER_WILL_BE_DELETE,
                        payload: currentRow
                    })
                    setOpenModalDeleteOrder(true)
                }

                return (
                    <Stack direction="row" spacing={2}>
                        <Button variant="outlined" color="success" size="small" onClick={onBtnUpdateClickHandler}>Edit</Button>
                        <Button variant="outlined" color="error" size="small" onClick={onBtnDeleteClickHandler} >Delete</Button>
                    </Stack>
                );
            },
        }



    ];
    const rows = orders;
    return (
        <>
            <Container maxWidth>
                <Grid container mt={5}>
                    <Grid item lg={12} md={12} sm={12} xs={12} textAlign="start">
                        <Button variant="contained" sx={{ mb: 2 }} onClick={() => { handleOpen() }}>Thêm đơn hàng</Button>
                    </Grid>
                    <Grid item lg={12} md={12} sm={12} xs={12} textAlign="center">
                        <h1 style={{ color: 'blue' }}>QUẢN LÝ ĐƠN HÀNG</h1>
                    </Grid>
                    <Grid item style={{ height: 700, width: '100%' }}>
                        {
                            pending ?
                                <Grid item lg={12} md={12} sm={12} xs={12} textAlign="center">
                                    < CircularProgress color="secondary" />
                                </Grid>
                                :
                                <DataGrid
                                    rows={rows}
                                    columns={columns}
                                    initialState={{
                                        pagination: {
                                            paginationModel: { page: 0, pageSize: 13 },
                                        },
                                    }}
                                    pageSizeOptions={[5, 10, 25, 50]}

                                />
                        }

                    </Grid>
                </Grid>



                <div>
                    <Modal
                        open={open}
                        onClose={handleClose}
                        aria-labelledby="modal-modal-title"
                        aria-describedby="modal-modal-description"

                    >
                        <Box sx={style}>
                            <Typography id="modal-modal-title"
                                variant="h6" component="h2">
                                THÊM ĐƠN HÀNG
                            </Typography>
                            <Grid container spacing={2}>
                                <Grid item container spacing={2} md={12}>
                                    <Grid item md={6}>
                                        <FormControl sx={{ width: '100%' }}>
                                            <InputLabel id="demo-simple-select-label">Select Size</InputLabel>
                                            <Select
                                                labelId="demo-simple-select-label"
                                                id="select-size"
                                                label="Select Size"
                                                onChange={handleChange}
                                                value={size}
                                            >
                                                <MenuItem value={"S"}>S</MenuItem>
                                                <MenuItem value={"M"}>M</MenuItem>
                                                <MenuItem value={"L"}>L</MenuItem>
                                            </Select>
                                        </FormControl>

                                    </Grid>
                                    <Grid item container md={6} spacing={2}>
                                        <Grid item md={12}>
                                            <Typography id="modal-modal-title"
                                                variant="h6" component="h2">
                                                Đường kính: {sizeSelected.duongKinh} cm
                                            </Typography>
                                        </Grid>
                                        <Grid item md={12}>
                                            <Typography id="modal-modal-title"
                                                variant="h6" component="h2">
                                                Số lượng sườn nướng: {sizeSelected.suon} cm
                                            </Typography>                                        </Grid>
                                        <Grid item md={12}>
                                            <Typography id="modal-modal-title"
                                                variant="h6" component="h2">
                                                Salad: {sizeSelected.salad} gram
                                            </Typography>                                         </Grid>
                                        <Grid item md={12}>
                                            <Typography id="modal-modal-title"
                                                variant="h6" component="h2">
                                                Số lượng nước uống: {sizeSelected.soLuongNuoc}
                                            </Typography>                                         </Grid>
                                        <Grid item md={12}>
                                            <Typography id="modal-modal-title"
                                                variant="h6" component="h2">
                                                Đơn giá: {sizeSelected.suon} VND
                                            </Typography>                                         </Grid>


                                    </Grid>
                                </Grid>


                                <Grid item md={4}>
                                    <FormControl sx={{ width: '100%' }}>

                                        <InputLabel id="lable-select-pizza-type">Select Pizza</InputLabel>
                                        <Select
                                            labelId="lable-select-pizza-type"
                                            id="select-priza-type"
                                            label="Select Pizza"
                                            onChange={pizzaTypeSelectHandler}
                                            value={pizzaType}
                                        >
                                            <MenuItem value={'Seafood'}>Hải sản</MenuItem>
                                            <MenuItem value={'Hawaii'}>Hawaii</MenuItem>
                                            <MenuItem value={'Bacon'}>Thịt hun khói
                                            </MenuItem>
                                        </Select>
                                    </FormControl>

                                </Grid>
                                <Grid item md={4}>
                                    <TextField id="inp-id-voucher" label="Id voucher" variant="outlined" />
                                </Grid>
                                <Grid item md={4}>
                                    <FormControl sx={{ width: '100%' }}>
                                        {drinkPending ?
                                            <Grid item lg={12} md={12} sm={12} xs={12} textAlign="center">
                                                <CircularProgress color="secondary" />
                                            </Grid>
                                            :
                                            <>
                                                <InputLabel id="lable-select-drink">Select Drink</InputLabel>

                                                <Select
                                                    labelId="lable-select-drink"
                                                    id="select-drink"
                                                    label="Select Drink"
                                                    onChange={drinkSelectHandler}
                                                    value={drink}
                                                >
                                                    {drinks.map((drink, index) => {
                                                        return (
                                                            <MenuItem key={index} value={drink.maNuocUong}>{drink.tenNuocUong}</MenuItem>

                                                        )
                                                    })}
                                                </Select>
                                            </>
                                        }

                                    </FormControl>
                                </Grid>

                                <Grid item md={6}>
                                    <TextField sx={{ width: '100%' }} id="inp-full-name" label="Họ và tên" variant="outlined" />
                                </Grid>

                                <Grid item md={6}>
                                    <TextField sx={{ width: '100%' }} id="inp-email" label="Email" variant="outlined" />
                                </Grid>
                                <Grid item md={6}>
                                    <TextField sx={{ width: '100%' }} id="inp-phone" label="Số điện thoại" variant="outlined" />
                                </Grid>
                                <Grid item md={6}>
                                    <TextField sx={{ width: '100%' }} id="inp-address" label="Địa chỉ" variant="outlined" />
                                </Grid>
                                <Grid item md={12}>
                                    <TextField sx={{ width: '100%' }} id="inp-message" label="Lời nhắn" variant="outlined" />
                                </Grid>
                                <Grid item md={12}>
                                    <Button variant="contained" color="success" onClick={onBtnConfirmAddOrderClick}>Thêm</Button>
                                </Grid>
                            </Grid>
                        </Box>
                    </Modal>
                </div>
                <div>
                    <Modal
                        open={openModalUpdateOrder}
                        onClose={handleCloseModalUpdateOrder}
                        aria-labelledby="modal-modal-title"
                        aria-describedby="modal-modal-description"
                    >
                        <Box sx={style}>
                            <Grid container spacing={2}>
                                <Grid item md={12}>
                                    <Typography id="modal-modal-title"
                                        variant="h6" component="h2">
                                        SỬA ĐƠN HÀNG ID {updateOrder.id}
                                    </Typography>
                                </Grid>
                                <Grid item md={12}>
                                    <FormControl sx={{ width: '100%' }}>

                                        <InputLabel id="lable-select-order-status">Order status</InputLabel>
                                        <Select
                                            labelId="lable-select-order-status"
                                            id="select-order-status"
                                            label="Order status"
                                            onChange={orderStatusChangeHandler}
                                            value={orderStatus}
                                        >
                                            <MenuItem value={'open'}>Open</MenuItem>
                                            <MenuItem value={'cancel'}>Đã hủy</MenuItem>
                                            <MenuItem value={'confirmed'}>Đã xác nhận
                                            </MenuItem>
                                        </Select>
                                    </FormControl>
                                </Grid>
                                <Grid item md={12}>
                                    <Button variant="contained" color="success" onClick={onBtnConfirmUpdateOrderClick} >Sửa đơn hàng</Button>
                                </Grid>
                            </Grid>
                        </Box>
                    </Modal>
                </div>

                <div>
                    <Modal
                        open={openModalDeleteOrder}
                        onClose={handleCloseModalDeleteOrder}
                        aria-labelledby="modal-modal-title"
                        aria-describedby="modal-modal-description"
                    >
                        <Box sx={style}>
                            <Grid container spacing={2}>
                                <Grid item md={12}>
                                    <Typography id="modal-modal-title"
                                        variant="h6" component="h2">
                                        BẠN CÓ CHẮC CHẮN XÓA ĐƠN HÀNG ID {deleteOrder.id}

                                    </Typography>
                                </Grid>

                                <Grid item md={12}>
                                    <Button variant="contained" color="success" onClick={onBtnConfirmDeleteOrderClick}
                                    >Xóa đơn hàng</Button>
                                </Grid>
                            </Grid>
                        </Box>
                    </Modal>
                </div>
                <div>
                    {addOrderPending ?
                        <Grid item lg={12} md={12} sm={12} xs={12} textAlign="center">
                            <CircularProgress color="secondary" />
                        </Grid>
                        :
                        <Snackbar open={openSnackbar} autoHideDuration={2000} onClose={handleCloseSnackbar} >
                            <Alert severity="success" sx={{ width: '100%' }}>
                                Thêm đơn hàng mới id {newOrder.id} thành công!
                            </Alert>
                        </Snackbar>
                    }

                </div>

                <div>
                    <Snackbar open={openSnackbarUpdateOrder} autoHideDuration={2000} onClose={handleCloseSnackbarUpdateOrder} >
                        <Alert onClose={handleCloseSnackbarUpdateOrder} severity="info" sx={{ width: '100%' }}>
                            Update đơn hàng id {updateOrder.id} thành công!
                        </Alert>
                    </Snackbar>
                </div>

                <div>
                    <Snackbar open={openSnackbarDeleteOrder} autoHideDuration={2000} onClose={handleCloseSnackbarDeleteOrder} >
                        <Alert onClose={handleCloseSnackbarDeleteOrder} severity="error" sx={{ width: '100%' }}>
                            {orderDeleted}
                        </Alert>
                    </Snackbar>
                </div>
                <div>
                    <Snackbar open={openSnackbarValidateAddOrder} autoHideDuration={2000} onClose={handleCloseSnackbarValidateAddOrder} >
                        <Alert onClose={handleCloseSnackbarValidateAddOrder} severity="error" sx={{ width: '100%' }}>
                            {validateAlert}
                        </Alert>
                    </Snackbar>
                </div>

            </Container >
        </>
    )
}