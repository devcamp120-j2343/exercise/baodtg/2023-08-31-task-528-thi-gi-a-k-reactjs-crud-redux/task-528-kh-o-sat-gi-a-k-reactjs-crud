export const ORDER_FETCH_PENDING = "Trạng thái đợi khi gọi API lấy danh sách Order";

export const ORDER_FETCH_SUCCESS = "Trạng thái thành công khi gọi API lấy danh sách Order";

export const ORDER_FETCH_ERROR = "Trạng thái lỗi khi gọi API lấy danh sách Order";

export const S_SIZE_SELECTED = "Trạng thái hiển thị thông tin size S";

export const M_SIZE_SELECTED = "Trạng thái hiển thị thông tin size M";

export const L_SIZE_SELECTED = "Trạng thái hiển thị thông tin size L";

export const DRINK_FETCH_PENDING = "Trạng thái đợi khi gọi API lấy danh sách Drink";

export const DRINK_FETCH_SUCCESS = "Trạng thái thành công khi gọi API lấy danh sách Drink";

export const DRINK_FETCH_ERROR = "Trạng thái lỗi khi gọi API lấy danh sách Drink";


export const ADD_ORDER_FETCH_PENDING = "Trạng thái đợi khi gọi API tạo mới Order";

export const ADD_ORDER_FETCH_SUCCESS = "Trạng thái thành công khi gọi API tạo mới Order";

export const ADD_ORDER_FETCH_ERROR = "Trạng thái lỗi khi gọi API tạo mới Order";

export const GET_ORDER_WILL_BE_UPDATE = "Thu thập data order cần update";

export const ORDER_UPDATE_DATA = "Thu thập dữ liệu để update order";

export const UPDATE_ORDER_FETCH_PENDING = "Trạng thái đợi khi gọi API update Order";

export const UPDATE_ORDER_FETCH_SUCCESS = "Trạng thái thành công khi gọi API update Order";

export const UPDATE_ORDER_FETCH_ERROR = "Trạng thái lỗi khi gọi API update Order";

export const GET_ORDER_WILL_BE_DELETE = "Thu thập data order cần delete";

export const DELETE_ORDER_FETCH_PENDING = "Trạng thái đợi khi gọi API delete Order";

export const DELETE_ORDER_FETCH_SUCCESS = "Trạng thái thành công khi gọi API delete Order";

export const DELETE_ORDER_FETCH_ERROR = "Trạng thái lỗi khi gọi API delete Order";

