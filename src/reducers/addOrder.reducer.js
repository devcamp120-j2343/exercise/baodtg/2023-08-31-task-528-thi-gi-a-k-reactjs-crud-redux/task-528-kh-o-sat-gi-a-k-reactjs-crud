import { ADD_ORDER_FETCH_PENDING, ADD_ORDER_FETCH_SUCCESS } from "../constant/order.constant";


const initialState = {
    newOrder: [],
    addOrderPending: false,

}

export const addOrderReducer = (state = initialState, action) => {
    switch (action.type) {
        case ADD_ORDER_FETCH_PENDING:
            state.addOrderPending = true
            break;
        case ADD_ORDER_FETCH_SUCCESS:
            state.addOrderPending = false
            state.newOrder = action.data
            break;


        default:
            break;
    }


    return { ...state }
}