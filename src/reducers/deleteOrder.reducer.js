import { DELETE_ORDER_FETCH_PENDING, DELETE_ORDER_FETCH_SUCCESS, GET_ORDER_WILL_BE_DELETE } from "../constant/order.constant";

const initialState = {
    deleteOrder: [],
    deleteOrderPending: false,
    orderDeleted: "Xóa đơn hàng thành công !"
}


export const deleteOrderReducer = (state = initialState, action) => {
    switch (action.type) {
        case GET_ORDER_WILL_BE_DELETE:
            state.deleteOrder = action.payload
            break;

        case DELETE_ORDER_FETCH_PENDING:
            state.deleteOrderPending = true
            break;

        case DELETE_ORDER_FETCH_SUCCESS:
            state.deleteOrderPending = false;
            state.orderDeleted = state.orderDeleted
            break;

        default:
            break;
    }


    return { ...state }
}