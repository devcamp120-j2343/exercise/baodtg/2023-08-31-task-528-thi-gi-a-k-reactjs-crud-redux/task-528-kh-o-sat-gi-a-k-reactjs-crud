
import { DRINK_FETCH_PENDING, DRINK_FETCH_SUCCESS } from "../constant/order.constant";

const initialState = {
    drinks: [],
    drinkPending: false,

}

export const drinkReducer = (state = initialState, action) => {
    switch (action.type) {
        case DRINK_FETCH_PENDING:
            state.drinkPending = true
            break;
        case DRINK_FETCH_SUCCESS:
            state.drinkPending = false
            state.drinks = action.data
            break;


        default:
            break;
    }


    return { ...state }
}