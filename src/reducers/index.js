import { combineReducers } from "redux";

import { orderReducer } from "./order.reducer";
import { sizeReducer } from "./selectSize.reducer";
import { addOrderReducer } from "./addOrder.reducer";
import { drinkReducer } from "./drinkList.reducer";
import { updateOrderReducer } from "./updateOrder.reducer";
import { deleteOrderReducer } from "./deleteOrder.reducer";
export const rootReducer = combineReducers({
    orderReducer,
    sizeReducer,
    addOrderReducer,
    drinkReducer,
    updateOrderReducer,
    deleteOrderReducer
})