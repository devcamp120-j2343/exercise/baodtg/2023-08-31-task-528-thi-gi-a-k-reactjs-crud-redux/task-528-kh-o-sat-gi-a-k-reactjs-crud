import { ORDER_FETCH_PENDING, ORDER_FETCH_SUCCESS } from "../constant/order.constant";


const initialState = {
    orders: [],
    pending: false,

}

export const orderReducer = (state = initialState, action) => {
    switch (action.type) {
        case ORDER_FETCH_PENDING:
            state.pending = true
            break;
        case ORDER_FETCH_SUCCESS:
            state.pending = false
            state.orders = action.data;
            break;


        default:
            break;
    }


    return { ...state }
}