import { L_SIZE_SELECTED, M_SIZE_SELECTED, S_SIZE_SELECTED } from "../constant/order.constant";

const initialState = {
    sizeSelected: []
}
export const sizeReducer = (state = initialState, action) => {
    switch (action.type) {
        case S_SIZE_SELECTED:
            state.sizeSelected = action.payload
            break;
        case M_SIZE_SELECTED:
            state.sizeSelected = action.payload
            break;

        case L_SIZE_SELECTED:
            state.sizeSelected = action.payload

            break;
        default:
            break;
    }


    return { ...state }
}