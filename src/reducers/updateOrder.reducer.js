import { GET_ORDER_WILL_BE_UPDATE, ORDER_UPDATE_DATA, UPDATE_ORDER_FETCH_SUCCESS } from "../constant/order.constant";

const initialState = {
    updateOrder: [],
    orderStatus: "",
    orderUpdated: []
}


export const updateOrderReducer = (state = initialState, action) => {
    switch (action.type) {
        case GET_ORDER_WILL_BE_UPDATE:
            state.updateOrder = action.payload
            break;
        case ORDER_UPDATE_DATA:
            state.orderStatus = action.payload
            break;
        case UPDATE_ORDER_FETCH_SUCCESS:
            state.orderUpdated = action.data
            break;


        default:
            break;
    }


    return { ...state }
}